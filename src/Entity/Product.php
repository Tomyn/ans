<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $productCode = null;

    #[ORM\Column(length: 255)]
    private ?string $groupCode = null;

    #[ORM\Column(length: 255)]
    private ?string $showWeb = null;

    #[ORM\Column(length: 255)]
    private ?string $productName = null;

    #[ORM\Column]
    private array $categories = [];

    #[ORM\Column]
    private array $localizationIndexes = [];

    #[ORM\Column]
    private array $additionalInfo = [];

    #[ORM\Column]
    private array $descriptionParameters = [];

    #[ORM\Column(type: Types::TEXT)]
    private ?string $text = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductCode(): ?string
    {
        return $this->productCode;
    }

    public function setProductCode(string $productCode): self
    {
        $this->productCode = $productCode;

        return $this;
    }

    public function getGroupCode(): ?string
    {
        return $this->groupCode;
    }

    public function setGroupCode(string $groupCode): self
    {
        $this->groupCode = $groupCode;

        return $this;
    }

    public function getShowWeb(): ?string
    {
        return $this->showWeb;
    }

    public function setShowWeb(string $showWeb): self
    {
        $this->showWeb = $showWeb;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->productName;
    }

    public function setProductName(string $productName): self
    {
        $this->productName = $productName;

        return $this;
    }

    public function getCategories(): array
    {
        return $this->categories;
    }

    public function setCategories(array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function getLocalizationIndexes(): array
    {
        return $this->localizationIndexes;
    }

    public function setLocalizationIndexes(array $localizationIndexes): self
    {
        $this->localizationIndexes = $localizationIndexes;

        return $this;
    }

    public function getAdditionalInfo(): array
    {
        return $this->additionalInfo;
    }

    public function setAdditionalInfo(array $additionalInfo): self
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }

    public function getDescriptionParameters(): array
    {
        return $this->descriptionParameters;
    }

    public function setDescriptionParameters(array $descriptionParameters): self
    {
        $this->descriptionParameters = $descriptionParameters;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }
}
