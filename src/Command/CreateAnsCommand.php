<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\Repository\ProductRepository;
use App\Service\ProductManagerService;

class CreateAnsCommand extends Command
{
    private $pms;
    private $params;
    private $productRepository;

    public function __construct(
        ProductManagerService $pms,
        ParameterBagInterface $params,
        ProductRepository $productRepository,
        ) {
        $this->pms = $pms;
        $this->params = $params;
        $this->productRepository = $productRepository;
        parent::__construct();
    } 

    protected function configure(): void
    {
        $this
            ->setName('app:create-ans')
            ->setDescription('Creates product database');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->info(sprintf('Preparing database for products...'));
        $truncate = $this->productRepository->truncateTable();

        $io->info(sprintf('Creating database of products...'));
        $groupCodes = explode(',', $this->params->get('group_codes'));
        foreach($groupCodes as $groupCode) {
            $io->info(sprintf('Creating products for groupCode: ' . $groupCode));
            $result = $this->pms->createDatabaseOfProductsByGroupCode($groupCode);
            if (empty($result)) {
                $io->success(sprintf('Database of products for groupCode: ' . $groupCode . ' succesfully created.'));
            } else {
                $io->error(sprintf($result['message']));
                break;
            }    
        }

        $io->success(sprintf('Database of products succesfully created.'));

        return Command::SUCCESS;
    }
}