<?php
namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use App\Entity\Product;

class ProductManagerService
{
    private $em;
    private $client;
    private $params;
    
    public function __construct(
        EntityManagerInterface $em,
        HttpClientInterface $client,
        ParameterBagInterface $params,
        ) {
        $this->em = $em;
        $this->client = $client;
        $this->params = $params;
    } 
    
    public function createDatabaseOfProductsByGroupCode($groupCode): array
    {
        try {
            $url = $this->params->get('app_api');
            
            $products = $url . '/api/Products?expand=productCode,groupCode,showWeb,productName,text,additionalInfo,descriptionParameters';
            $productLocalizations = $url . '/api/ProductLocalizations?expand=productCode,culture,name,productInfo,productInfoWp,text,data,additionalInfo,descriptionParameters';
            //TODO zatím nepracuje s těmito endpointy
            $productEans = $url . '/api/ProductEans?expand=code,productCode,char1,charTab1';
            $productPrices = $url . '/api/ProductPrices?expand=productCode,stockCode,priceCode,validFrom,validTo,currencyCode,priceWithVat,priceWithoutVat';
            $productStockChars = $url . '/api/ProductStockChars?expand=productCode,stockCode,char1,accountQuantity,blockedQuantity,dateUpdated';
            
            $filter = '&filter=groupCode eq "' . $groupCode . '"';
            $response = $this->client->request('GET', $products . $filter);
            $statusCode = $response->getStatusCode();
            
            if($statusCode === 200) {
                $productsData = isset(json_decode($response->getContent(), true)['data']) ? json_decode($response->getContent(), true)['data'] : [];
                foreach($productsData as $productData) {
                    $productCode = (isset($productData['productCode'])) ? $productData['productCode'] : "";
                    $productName = (isset($productData['productName'])) ? $productData['productName'] : "";
                    $text = (isset($productData['text'])) ? $productData['text'] : "";
                    $additionalInfo = (isset($productData['additionalInfo'])) ? $productData['additionalInfo'] : [];
                    $showWeb = (isset($additionalInfo['showWeb'])) ? $additionalInfo['showWeb'] : "";
                    $descriptionParameters = (isset($productData['descriptionParameters'])) ? $productData['descriptionParameters'] : [];

                    $filter = '&filter=productCode eq "' . $productCode . '"';
                    $response = $this->client->request('GET', $productLocalizations . $filter);
                    $statusCode = $response->getStatusCode();
            
                    if($statusCode === 200) {
                        $productLocalizationsData = json_decode($response->getContent(), true)['data'];
                        
                        $found_key = array_search('de', array_column($productLocalizationsData, 'culture'));
                        $localizationIndexes = [];
                        $categories = [];
                        if(isset($productLocalizationsData[$found_key])) {
                            $dP = $productLocalizationsData[$found_key]['descriptionParameters'];

                            $key20 = array_search('20', array_column($dP, 'index'));
                            $key19 = array_search('19', array_column($dP, 'index'));
                            $key21 = array_search('21', array_column($dP, 'index'));
                            $key13 = array_search('13', array_column($dP, 'index'));

                            $categories = [
                                '20' => (isset($dP[$key20]['value'])) ? $dP[$key20]['value'] : "",
                                '19' => (isset($dP[$key19]['value'])) ? $dP[$key19]['value'] : "",
                                '21' => (isset($dP[$key21]['value'])) ? $dP[$key21]['value'] : "",
                                '13' => (isset($dP[$key13]['value'])) ? $dP[$key13]['value'] : "",

                            ];

                            $key14 = array_search('14', array_column($dP, 'index'));
                            $key0 = array_search('0', array_column($dP, 'index'));
                            $key11 = array_search('11', array_column($dP, 'index'));
                            $key12 = array_search('12', array_column($dP, 'index'));
                            $key26 = array_search('26', array_column($dP, 'index'));
                            $key29 = array_search('29', array_column($dP, 'index'));
                            $key30 = array_search('30', array_column($dP, 'index'));

                            $localizationIndexes = [
                                '14' => (isset($dP[$key14]['value'])) ? $dP[$key14]['value'] : "",
                                '0' => (isset($dP[$key0]['value'])) ? $dP[$key0]['value'] : "",
                                '11' => (isset($dP[$key11]['value'])) ? $dP[$key11]['value'] : "",
                                '12' => (isset($dP[$key12]['value'])) ? $dP[$key12]['value'] : "",
                                '26' => (isset($dP[$key26]['value'])) ? $dP[$key26]['value'] : "",
                                '29' => (isset($dP[$key29]['value'])) ? $dP[$key29]['value'] : "",
                                '30' => (isset($dP[$key30]['value'])) ? $dP[$key30]['value'] : "",
                            ];
                        }
                    }

                    $product = new Product();
                    $product->setProductCode($productCode);
                    $product->setGroupCode($groupCode);
                    $product->setShowWeb($showWeb);
                    $product->setProductName($productName);
                    $product->setText($text);
                    $product->setAdditionalInfo($additionalInfo);
                    $product->setDescriptionParameters($descriptionParameters);
                    $product->setCategories($categories);
                    $product->setLocalizationIndexes($localizationIndexes);
                    
                    $this->em->persist($product);
                    $this->em->flush();
                }
            }
        } catch (IOExceptionInterface $exception) {
            return ['message' => "An error occurred while creating your products database at ".$exception];
        }

        return [];
    }
}